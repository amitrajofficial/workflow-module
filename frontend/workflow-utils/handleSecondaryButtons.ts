import { ISecondaryButton, IWorkflowFormGenerator } from 'common/workflow-components/workflow-form-generator/interfaces';

export const handleSecondaryButtonClick = (button: ISecondaryButton, props: IWorkflowFormGenerator) => {
    const { action, title } = button;
    const { name } = action;
    const { closeModal, onSubmit, hiddenValues } = props;
    if ( name === "close" && closeModal && action.next ) {
        onSubmit({ selectedResponse: title }, action.next, hiddenValues );
        closeModal();
    } else if ( name === "submit" ) {
        onSubmit( { selectedResponse: title }, action.next, hiddenValues );
    }

} 