import * as qsParser from "query-string";

import { get as _get, debounce } from 'lodash';
import { startSocketConnection, stopSocketConnection } from './socketsUtils';
import { useEffect, useState } from 'react';

import { WORKFLOWS_SERVER_URL } from 'app.config';
import socketIOClient from "socket.io-client";
import { trackJsLogger } from 'utils/trackJsLogger';

export const useGetWorkflowHooks = ({ name, data, location }) => {
    const socketConnection = socketIOClient(
        WORKFLOWS_SERVER_URL,
        {
            forceNew: true,
            reconnectionAttempts: 10,
        }
    );
    const [ socket ] = useState(socketConnection);
    const [ errorTemplate, changeErrorTemplate ] = useState({ error: { type: '', message: '' }, prev: null });
    const [ workflowMetaTabs, changeWorkflowMetaTabs ] = useState({
        workflowMeta: {
            name: null,
            data: null
        },
        workflowTabs: []
    });
    const [loading, setLoading] = useState('');
    const [ reconnect, changeReconnect ] = useState({ connectionLost: false, reconnectTry: 0 });
    const values = location
        ? qsParser.parse(location.search)
        : null;
    const propData = {
        name: (_get( values, 'workflow_name', name )) || name || null,
        data: (values && typeof values.data === "string" && JSON.parse(values.data)) || data || {}
    };

    const handleSocketData = debounce(
        (err, convertedData, reconnect) => {
            trackJsLogger('printing socket data', err, convertedData, reconnect);
        if (!err) {
            changeWorkflowMetaTabs({
                workflowMeta: {
                    name: _get(convertedData, 'currentWorkflow'),
                    data: propData.data
                },
                workflowTabs: _get(convertedData, 'workflowTabs', [])
            });
            changeErrorTemplate({ error: { type: '', message: '' }, prev: null });
        } else if (reconnect) {
            changeReconnect(reconnect);
            changeErrorTemplate({ error: { type: '', message: '' }, prev: null });
        } else {
            changeErrorTemplate(err);
        }
        setLoading('');
    });
    useEffect(() => {
        setLoading('close');
        startSocketConnection(socket, propData, handleSocketData);
        // attachOnStep(workflowMetaTabs.workflowMeta.name, handleSocketData);
        return () => {
            stopSocketConnection(socket);
        }
    }, []);
    return { errorTemplate, ...workflowMetaTabs,reconnect,loading,setLoading, socket };
}