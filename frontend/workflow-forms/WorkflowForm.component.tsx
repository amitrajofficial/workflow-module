import React, { FC, memo } from 'react';
import { WithFallback, WorkflowError, WorkflowFallback, WorkflowMutitab, WorkflowSingleTab } from '../workflow-components';
import { goBack, onSubmit } from './socketsUtils';

import { WORKFLOW_TYPES } from "./interfaces";
import { get as _get } from 'lodash';
import { useGetWorkflowHooks } from './workflow-hooks';

export const WorkFlowForm: FC<WORKFLOW_TYPES> = ({
        location,
        name,
        data,
        closeModal
    }) => {
    console.log("INSIDE WorkFlowForm");
    const { workflowMeta, workflowTabs, errorTemplate, loading, setLoading, socket } = useGetWorkflowHooks({ name, data, location });
    const tabLength = workflowTabs.length;
    const hasError = _get(errorTemplate, 'error.type', null) && JSON.stringify(_get(errorTemplate, 'error.type', null)).length > 0;
    // let dummyClose = null;
    if(!closeModal) {
      closeModal = () => {
        setLoading('close');
        const workflowStepName = workflowTabs && workflowTabs[0] && workflowTabs[0].formSchema && workflowTabs[0].formSchema.name;
        const userVertical = workflowMeta && workflowMeta.data && workflowMeta.data.userVertical;
        // workflowStepName used to track on which step modal was closed by user
        if(workflowStepName){
          window.parent.postMessage({ workflowStepName, userVertical }, "*");
        } else {
          window.parent.postMessage("close", "*");
        }
        // send message to angular
      }
    }
    if ( hasError ) {
      return (
        <WithFallback>
          <WorkflowError
            goBack={() => {
                if(_get(errorTemplate, 'prev.name') ){
                  setLoading('back');
                  goBack(socket, workflowMeta, errorTemplate)
                }
            }}
            loading = {loading}
            errorTemplate={ errorTemplate }
            closeModal={closeModal}
          />
        </WithFallback>
      )
    } else if ( tabLength === 1 ) {
      return (
        <WithFallback>
          <WorkflowSingleTab
            goBack={() => {setLoading('back'); goBack(socket, workflowMeta, workflowTabs[0])}}
            workflowTab={workflowTabs[0]}
            loading= {loading}
            onSubmit={ (values, next, hiddenValues) => {setLoading(values.selectedResponse); onSubmit( socket, workflowMeta, values, next || _get(workflowTabs[0], 'next'), hiddenValues)} }
            closeModal={closeModal}
          />
        </WithFallback>
      );
    } else if ( tabLength > 1 ) {
      return (
        <WithFallback>
          <WorkflowMutitab
            goBack={() => {setLoading('back'); goBack(socket, workflowMeta, workflowTabs[0])}}
            workflowTabs={workflowTabs}
            loading = {loading}
            onSubmit={ (values, next, hiddenValues) => {setLoading(values.selectedResponse); onSubmit( socket, workflowMeta, values, next || _get(workflowTabs[0], 'next'), hiddenValues)} }
            closeModal={closeModal}
          />
        </WithFallback>
      );
    }
    return (
      <WorkflowFallback />
    );
}

export default memo(WorkFlowForm);