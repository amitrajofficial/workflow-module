import { IWorkflowErrorSchema, IWorkflowResponseData } from './interfaces';
import { debounce, omit } from 'lodash';

import convertToYupObj from "./convertToYupSchema";
import { getUserAttributesFromCookies } from 'utils/user/getUserFromCookies';

// import socketIOClient from "socket.io-client";


const authorization = (socketConnection, cb) => {
    // in user profile socketConnection will be giving {jwt, profile, userName, userId, userEmailId}
    const jwt = getUserAttributesFromCookies('jwt');
    const profile = getUserAttributesFromCookies('profile');
    const name = getUserAttributesFromCookies('first_name') || "" + " " + getUserAttributesFromCookies('last_name') || "";
    const id = getUserAttributesFromCookies('id');
    const emailId = getUserAttributesFromCookies('email');
    socketConnection.emit("authorization", { jwt, profile, name, id, emailId }, cb);
}

const emitWorkflow = (socketConnection, name, data, cb) => {
    socketConnection.emit("workflow", name, data, cb);
}

const onWorklowStep = debounce(
    (socketConnection, step, cb) => {
    socketConnection.on( step, cb );
    
})

const convertData = data => data.map(comp => {
    return omit({
      formSchema: comp.form,
      validationSchema: convertToYupObj(comp.validators, null),
      ...comp
    }, ['form', 'validators']);
});

export const startSocketConnection = (socketConnection, { name, data }, cb,  ) => {
    socketConnection.open();
    // set user authorization
    authorization(socketConnection, authorized => {
        // if user is set on backend get workflow steps
         authorized && emitWorkflow(socketConnection, name, data, workflowStep => {
             // get current step data
             attachOnStep(socketConnection, workflowStep, cb);
        })
    });
    // handle errors
    onError(socketConnection, cb);
    connectError(socketConnection, cb);
    reconnectAttempts(socketConnection, cb);
};

export const onSubmit = (socketConnection, { name, data }, values, next, hiddenValues ) => {
    socketConnection.emit( name, values, data, next, hiddenValues );
};

export const goBack = (socketConnection, { name, data }, tab: IWorkflowResponseData | IWorkflowErrorSchema ) => {
    socketConnection.emit(
      name,
      tab.prev.values,
      data,
      tab.prev.name
    );
}

export const onError = (socketConnection, cb) => {
    socketConnection.on('errors', cb);
}

export const stopSocketConnection = (socketConnection) => {
    socketConnection.close();
}

export const connectError = (socketConnection, cb) => {
    socketConnection.on('connect_error', () => {
        cb(null, null, { connectionLost: true, reconnectTry: 0 });
    })
}

export const reconnectAttempts = (socketConnection, cb) => {
    // on reconnection, reset the transports option, as the Websocket
    // connection may have failed (caused by proxy, firewall, browser, ...)
    socketConnection.on('reconnect_attempt', (num) => {
        socketConnection.io.opts.transports = ['polling', 'websocket'];
        cb(null, null, {
            connectionLost: true,
            reconnectTry: num
        });
    });
}

export const attachOnStep = (socketConnection, workflowStep, cb) => {
    workflowStep && onWorklowStep(socketConnection, workflowStep, stepData => {
        // after receiving step data run callback
        stepData && cb ( null,
            {
               workflowTabs: convertData(stepData),
               currentWorkflow: workflowStep
           },
           {
               connectionLost: false,
               reconnectTry: 0
           }
        )
   });
}