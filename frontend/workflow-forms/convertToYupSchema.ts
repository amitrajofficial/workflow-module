import * as _ from "lodash";

import Yup from "yup";

const prioritySequence = ["regex", "url", "required"];

const getStringValidationTypes = (name: string, value: any) => {
  let yupType, callValue;
  switch (name) {
    case "regex":
      yupType = "matches";
      callValue = [new RegExp(value), "no regex match"];
      break;
    case "url":
      yupType = "url";
      callValue = null;
      break;
    case "required":
      yupType = "required";
      callValue = ["is required"];
      break;
    default:
      yupType = name;
      callValue = null;
  }
  return {
    type: yupType,
    callWith: callValue
  };
};

const getValidationTypes = (type: string, properties: any) => {
  let yupType;
  switch (type) {
    case "string":
      yupType = Yup.string();
      let propertyKeys = Object.keys(properties);
      propertyKeys = prioritySequence.filter(props => {
        return propertyKeys.indexOf(props) > -1;
      });
      console.log("propertyKeys", propertyKeys);
      propertyKeys.forEach(propName => {
        debugger;
        const toCall = getStringValidationTypes(propName, properties[propName]);
        console.log("toCall.type", toCall.type);
        yupType = yupType[toCall.type].apply(
          yupType,
          toCall.callWith || undefined
        );
      });
      break;
    default:
    // yupType = Yup.object();
  }
  return yupType.nullable();
};

const getYupObj = function(validationObject: any, key: any) {
  let validator = {};
  function getValidator(validatorSection: any) {
    for (let attr in validatorSection) {
      if (validatorSection.hasOwnProperty(attr)) {
        if (attr !== "$_type" && attr !== "$_properties") {
          const childKeys = Object.keys(validatorSection[attr]);
          if (
            childKeys.indexOf("$_type") === -1 &&
            childKeys.indexOf("$_properties") === -1
          ) {
            if (key) {
              validator[key] = Yup.object().shape(
                getYupObj(validatorSection[attr], attr)
              );
            } else {
              validator[attr] = Yup.object().shape(
                getYupObj(validatorSection[attr], null)
              );
            }
          } else {
            const YupType = getValidationTypes(
              validatorSection[attr].$_type,
              validatorSection[attr].$_properties
            );
            if (key) {
              validator[key] = YupType;
            } else {
              validator[attr] = YupType;
            }
          }
        } else {
          const YupType = getValidationTypes(
            validatorSection.$_type,
            validatorSection.$_properties
          );
          if (key) {
            validator[key] = YupType;
          } else {
            validator = YupType;
          }
        }
      }
    }
  }
  getValidator(validationObject);
  return validator;
};

const convertToYupObj = function(validator: any, key: any) {
  return Yup.object().shape(getYupObj(validator, null));
};

export default convertToYupObj;
