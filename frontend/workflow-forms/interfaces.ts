// import { WorkflowEnums } from "./enums";
export interface IWorkflowResponseData {
  formSchema: any;
  validationSchema: any;
  values: any;
  information?: string | null;
  next: string | null;
  title?: string | number | null;
  prev?: any;
  error?: {
    type: string;
    message: string;
  }
}
export interface IWorkflowFormState {
  workflowTabs: Array<IWorkflowResponseData>;
  currentWorkflow: string | null;
  propData: any;
  errorTemplate: IWorkflowErrorSchema
}

export interface IWorkflowErrorSchema {
  error: {
    type: string | number;
    message?: string
  };
  information?: string;
  prev: {
    name: string;
    values: { [propName: string]: any };
  } | null;
}
interface IBookInterviewData {
  application_id: string;
  job_id: string;
}

export type WORKFLOW_TYPES = {
  name: string;
  data: IBookInterviewData;
  closeModal: () => void;
  location?: any;
};
