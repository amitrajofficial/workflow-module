import { Button, Icon } from 'antd';
import React, { FC, memo } from 'react';

export const WorkflowGoBackButton: FC<any> = ({ goBack, loading }) => (
    <Button onClick={goBack} loading={loading === 'back'}>
      <Icon type="left" />
      Back
    </Button>
);

export default memo(WorkflowGoBackButton);