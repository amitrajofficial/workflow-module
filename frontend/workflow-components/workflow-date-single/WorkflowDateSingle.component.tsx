import React, {FC, memo, useState} from "react";

import { DatePicker } from "antd";
import _ from 'lodash';
import moment from "moment";

// import * as moment from "moment";

const WorkflowDateSingle: FC<any> = ({
    fieldDecoratorProps,
    type,
    form,
    ref
}) => {
    let {options} = type;
    const { displayFormat,valueFormat, disableBetween,enableBetween, enableBetweenRange } = options;
    const {value,onChange, ...rest }  = fieldDecoratorProps;
    const [dateValue,setDateValue] = useState(value);
    const disabledDateCondition = (current) => {
        if(enableBetween) {
            let forward; 
            let backward;
            if(enableBetween.forward)
                forward = current >= (moment as any)().add(_.get(enableBetween,"forward.value",null), _.get(enableBetween,"forward.type","days"))
            if(enableBetween.back)
                backward = current <= (moment as any)().subtract(_.get(enableBetween,"back.value",null), _.get(enableBetween,"back.type","days"))

            return (
                current && (forward || backward)
                )
        }
        else if( disableBetween) {
            let forward = null; 
            let backward = null;
            if(disableBetween.forward)
                forward = current <= (moment as any)().add(_.get(disableBetween,"forward.value",null), _.get(disableBetween,"forward.type","days")).endOf("day")
            if(disableBetween.back)
                backward = current >= (moment as any)().subtract(_.get(disableBetween,"back.value",null), _.get(disableBetween,"back.type","days")).endOf("day")
            
            if(forward!==null && backward!== null)
                return (current && forward && backward)
            else if(forward!==null)
                return (current && forward)
            else if(backward!==null)
                return (current && backward)
        }
        if ( enableBetweenRange ) {
            return (
                current > (moment as any)().subtract(_.get(enableBetweenRange,"min.value",null), _.get(enableBetweenRange,"min.type","days")) ||
                current < (moment as any)().subtract(_.get(enableBetweenRange,"max.value",null), _.get(enableBetweenRange,"max.type","days"))
            )
        }
        return ( null )
    }

    const onChangeHandler = (event) => {
        if(dateValue !== value)
            onChange(moment(dateValue).format(valueFormat || "YYYY-MM-DD"));
    }
    
    return (
        <DatePicker
            value = {dateValue ? moment(dateValue) : null}
            showTime = {false}
            onChange = {setDateValue}
            format={displayFormat || "YYYY-MM-DD"}
            disabledDate = {disabledDateCondition}
            ref = {ref}
            onBlur = {onChangeHandler}
            allowClear = {false}
            style={{ width: '100%' }}
            {...rest}
        />
    );
};
export default memo(WorkflowDateSingle);
