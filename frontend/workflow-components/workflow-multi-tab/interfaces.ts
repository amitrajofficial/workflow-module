export interface IWorkflowResponseData {
    formSchema: any;
    validationSchema: any;
    values: any;
    information?: string | null;
    next: string | null;
    title?: string | number | null;
    prev?: any;
    error?: {
      type: string;
      message: string;
    }
}

export interface IWorkflowMultiTab {
    goBack: (event) => void;
    workflowTabs: Array<IWorkflowResponseData>;
    loading?: string;
    onSubmit: Function;
    closeModal: () => void;
}