import React, { FC, memo } from 'react';

import { IWorkflowMultiTab } from './interfaces';
import { Tabs } from "antd";
import { WorkflowSingleTab } from 'common/workflow-components';

const TabPane = Tabs.TabPane;

const WorkflowMutitab: FC<IWorkflowMultiTab> = ({ goBack, onSubmit, workflowTabs, closeModal, loading }) => (
    <div className="card-container">
        <Tabs type="card">
            {
                workflowTabs.map((tab, index) => (
                    <TabPane tab={tab.title || "Tab " + index} key={`${index}`}>
                        <WorkflowSingleTab
                            goBack={goBack}
                            workflowTab={tab}
                            loading= {loading}
                            onSubmit={onSubmit}
                            closeModal={closeModal}
                        />
                    </TabPane>
                ))
            }
        </Tabs>
    </div>
);

export default memo(WorkflowMutitab);