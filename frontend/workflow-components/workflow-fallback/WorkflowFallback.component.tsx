import React, { FC } from 'react';

import { Skeleton } from 'antd';

const WorkflowFallbackComponent: FC<any> = () => (
    <Skeleton paragraph={{ rows: 2 }} className='padding-10'/>
);

export default WorkflowFallbackComponent;
