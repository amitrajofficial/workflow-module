import React, { FC } from 'react';

import Fields from 'common/workflow-components/workflow-form-generator/Fields.component';
import { IComponentProps } from 'common/workflow-components/components.interface';
import SectionTemplate from 'common/workflow-components/workflow-form-generator/SectionTemplate.component';
import { get } from 'lodash';
import handleDependency from 'common/workflow-components/workflow-childrens/index';

const WorkflowDynamicComponent: FC<IComponentProps> = ({
    fieldDecoratorProps,
    type,
    form
}) => {
    const { options } = type;
    const { parent, connection } = options;
    const Children = handleDependency[connection];
    return (
        <>
        {
            get(options, 'parent.type.widget') !=="empty" &&
            <SectionTemplate {...parent} key={ get(parent, 'key', '') }>
                
                    <Fields
                        type={ get(parent, 'type', null) }
                        mappingKey={ get(parent, 'key', '') }
                        customValidation={ get(parent, 'validator', null) }
                        form={form}
                    />
            </SectionTemplate>
        }

            {
                Children && 
                <Children
                    options={options}
                    form={form}
                    fieldDecoratorProps={fieldDecoratorProps}
                />
            }
        </>
    )
};

export default WorkflowDynamicComponent;