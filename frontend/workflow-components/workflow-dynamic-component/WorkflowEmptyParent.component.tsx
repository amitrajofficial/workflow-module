import React, { FC } from 'react';

import { IComponentProps } from '../../../common/workflow-components/components.interface';

const WorkflowEmptyParent: FC<IComponentProps> = () => {
    return (
        <></>
    )
}

export default WorkflowEmptyParent;