export interface IWorkflowResponseData {
    slice: {
      formSchema: any;
      validationSchema: any;
      values: any;
      information?: string | null;
      next: string | null;
      title?: string | number | null;
      prev?: any;
      error?: {
        type: string;
        message: string;
      };
      secondaryButtons?: Array<object>;
      customData?: any;
      cssClass?: string;
      submitButtonText?: string;
    };
    onSubmit: Function;
    closeModal: Function;
    loading?: string;
}
  