import { Col, Result, Row } from 'antd';
import React, {FC, memo} from "react";

import { IWorkflowResponseData } from './interfaces';
import ReactMarkdown from 'react-markdown/with-html';
// import { WorkflowFormGenerator } from "common/workflow-form-generator/WorkflowFormGenerator.component";
import { WorkflowFormGenerator } from "common/workflow-components";
import { get as _get } from 'lodash';

export const WorkflowIndividualTab:FC<IWorkflowResponseData> = ({ slice, onSubmit, closeModal, loading }) => {
    const info = _get(slice, 'information');
    const status = _get(slice,'status');
    let extra = _get(slice,'extra', null);
    const next = _get(slice,'next', null);
    const hiddenValues = _get(slice,'hiddenValues', null);
    return (
      <Row>
        {
          info && next!== null ? 
          <Col span={24} className="workflow-info-section workflow-form-padding">
            <ReactMarkdown source={_get(slice, 'information')} escapeHtml={false}/>
          </Col> : null
        }
        {
          info && next=== null && status ?
          <Col span={24} className="workflow-form-padding">
            { <Result status = {status || "info"} title = {info} extra = {<div dangerouslySetInnerHTML ={{ __html: extra}}></div>}/>}
          </Col> : null
        }
        {
          !info && !next && !status && !_get(slice, 'formSchema')?
          <Col span={24} className="workflow-form-padding">
            { <Result status = {"error"} title = {"Data Missing"}/>}
          </Col> : null
        }

        <Col span={24} className="ant-card-body">
          <WorkflowFormGenerator
            schema={_get(slice, 'formSchema')}
            validationSchema={_get(slice, 'validationSchema')}
            onSubmit={ onSubmit }
            values={_get(slice, 'values')}
            secondaryButtons={_get(slice, 'secondaryButtons')}
            customData={_get(slice, 'customData')}
            cssClass={_get(slice, 'cssClass')}
            submitButtonText={_get(slice, 'submitButtonText')}
            closeModal={closeModal}
            next={_get(slice, 'next')}
            loading= {loading}
            status = {status}
            disableAutomaticClose = {_get(slice,'disableAutomaticClose')}
            hiddenValues =  {hiddenValues}
          />
        </Col>
      </Row>
    );
};

export default memo(WorkflowIndividualTab);
