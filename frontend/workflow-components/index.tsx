import React, { FC, ReactElement, Suspense, lazy } from 'react';

import WorkflowFallback from './workflow-fallback/WorkflowFallback.component';
import handleDependency from "./workflow-childrens/index";

const WorkflowCheckbox = lazy(() => import("./workflow-checkbox/WorkflowCheckbox.component"));
const WorkflowChecklist = lazy(() => import("./workflow-checklist/WorkflowChecklist.component"));
const WorkflowCloseButton = lazy(() => import("./workflow-close-button/WorkflowCloseButton.component"));
const WorkflowDateSingle = lazy(() => import("./workflow-date-single/WorkflowDateSingle.component"));
const WorkflowDateTime = lazy(()=> import("./workflow-date-single/WorkflowDateTime.component"));
const WorkflowDateRange = lazy(() => import("./workflow-date-range/WorkflowDateRange.component"));
const WorkflowDateRangeTime = lazy(()=> import("./workflow-date-range/WorkflowDateRangeTime.component"));
const WorkflowTimePicker = lazy(() => import("./workflow-time-picker/WorkflowTimePicker.component"));
const WorkflowDropdown = lazy(() => import("./workflow-dropdown/WorkflowDropdown.component"));
const WorkflowDynamicComponent = lazy(() => import("./workflow-dynamic-component/WorkflowDynamic.component"));
const WorkflowError = lazy(() => import("./workflow-error/WorkflowError.component"));
const WorkflowGoBackButton = lazy(() => import("./workflow-go-back-button/WorkflowGoBackButton.component"));
const WorkflowIndividualTab = lazy(() => import("./workflow-individual-tabs/WorkflowIndividualTabs.component"));
const WorkflowInput = lazy(() => import("./workflow-input/WorkflowInput.component"));
const WorkflowInputNumber = lazy(() => import("./workflow-input-number/WorkflowInputNumber.component"));
const WorkflowMutitab = lazy(() => import("./workflow-multi-tab/WorkflowMultiTab.component"));
const WorkflowRadioButton = lazy(() => import("./workflow-radio-button/WorkflowRadioButton.component"));
const WorkflowSingleTab = lazy(() => import("./workflow-single-tab/WorkflowSingleTab.component"));
const WorkflowFormGenerator = lazy(() => import("./workflow-form-generator/WorkflowFormGenerator.component"));
const WorkflowTextArea = lazy(() => import("./workflow-text-area/WorkflowTextArea.component"));
const WorkflowButton = lazy(() => import('./workflow-button/WorkflowButton.component'));
const WorkflowTypeAhead = lazy(()=> import("./workflow-typeahead/WorkflowTypeAhead.component"));
const WorkflowEmptyParent = lazy(()=> import('./workflow-empty-parent/WorkflowEmptyParent.component'));

const WithFallback:FC<{ children: ReactElement; }> = ({ children }) => (
  <Suspense fallback={<WorkflowFallback />}>
    { children }
  </Suspense>
);

export {
  WorkflowDropdown,
  WorkflowInput,
  WorkflowDateSingle,
  WorkflowDateTime,
  WorkflowDateRange,
  WorkflowDateRangeTime,
  WorkflowTimePicker,
  WorkflowInputNumber,
  WorkflowChecklist,
  WorkflowCheckbox,
  WorkflowRadioButton,
  WorkflowDynamicComponent,
  WorkflowError,
  WorkflowIndividualTab,
  WorkflowSingleTab,
  WorkflowMutitab,
  WorkflowGoBackButton,
  WorkflowCloseButton,
  WorkflowFallback,
  WorkflowFormGenerator,
  WithFallback,
  handleDependency,
  WorkflowTextArea,
  WorkflowButton,
  WorkflowTypeAhead,
  WorkflowEmptyParent
};
