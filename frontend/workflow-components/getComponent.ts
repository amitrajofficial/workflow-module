import {
  WorkflowButton,
  WorkflowCheckbox,
  WorkflowChecklist,
  WorkflowDateRange,
  WorkflowDateRangeTime,
  WorkflowDateSingle,
  WorkflowDateTime,
  WorkflowDropdown,
  WorkflowDynamicComponent,
  WorkflowInput,
  WorkflowInputNumber,
  WorkflowRadioButton,
  WorkflowTextArea,
  WorkflowTimePicker,
  WorkflowTypeAhead,
  WorkflowEmptyParent,
} from "common/workflow-components";

import { IWidgetType } from "common/form-generator/Generator.component";

const components = {
  'checkbox': WorkflowCheckbox,
  'date-single': WorkflowDateSingle,
  'date-time': WorkflowDateTime,
  'date-range': WorkflowDateRange,
  'date-range-time': WorkflowDateRangeTime,
  'time-picker': WorkflowTimePicker,
  'dropdown': WorkflowDropdown,
  'input-number': WorkflowInputNumber,
  'checklist': WorkflowChecklist,
  'radio': WorkflowRadioButton,
  'dynamic': WorkflowDynamicComponent,
  'text-area': WorkflowTextArea,
  'input': WorkflowInput,
  'button': WorkflowButton,
  "type-ahead": WorkflowTypeAhead,
  "empty": WorkflowEmptyParent
}

const getComponent = (type?: IWidgetType) => {
  return components[type.widget] || components['input'];
};

export default getComponent;
