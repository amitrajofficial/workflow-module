import React, {FC, memo, useState, useEffect} from "react";
import { Select, Spin } from "antd";
import _, { debounce, map, get as _get } from 'lodash';
import { gQLFetch } from "utils/glqFetch";
import { get } from "utils/server-request/apiRequest";
import { replaceDelimiter } from "utils/utilities";
const Option = Select.Option;

const WorkflowTypeAhead: FC<any> = ({
    fieldDecoratorProps,
    type,
    form
}) => {
    //TODO: complete this typeAhead because, value to show and value to store may be change.
    let {options} = type;
    const {mode , placeholder, query, pathToData, labelKey, valueKey} = options;
    // const {value, onChange, ...rest} = fieldDecoratorProps;
    let [dataSrc, setDataSrc] = useState(options.items || []);
    let [fetching] = useState(false);
    const transformData = (response: any) => {
        let valueKeys = valueKey;
        !valueKeys?valueKeys="value":valueKeys=valueKey;
        
        return _get(response,pathToData,[]).map(element => ({
            label: _get(element,(labelKey || "key")),
            value: typeof(valueKeys === "string")? _get(element,valueKeys) : valueKey.map(key => _get(element,key)).join('|')
        }))
    }
    useEffect(()=> {
        const prefetchVariables = _get(options,'prefetch');
        if(prefetchVariables && fieldDecoratorProps.value){
            options.isRestApi? debounce(createCall, 300)(null, get, prefetchVariables): createCall(null,gQLFetch, prefetchVariables);
        }
    },[])
    const createCall = (str, fn, variablesWithDelimiter) => {
        const value = fieldDecoratorProps.value;
        const variables = variablesWithDelimiter ? replaceDelimiter(variablesWithDelimiter, { '$$str': str, '$$value': value }) : {};
        if(str || value) {
            fn(query, {...variables}).then(response => {
                let transformedData = transformData(response);
                let labelKey = "label"
                let valueKey = "value"
                let dataSrc = transformedData.map(current => {
                    return {
                        ...current,
                        label__ui: current[labelKey],
                        value__ui: current[valueKey]
                    };
                });
                setDataSrc(dataSrc);
            })
        }
    }
    const search = (str: string) => {
        if(query)
        {
            const gqlVariables = {str, ...options.variables};
            const restVariables = {...options.variables};
            options.isRestApi? createCall(str, get, restVariables) :createCall(str,gQLFetch, gqlVariables);
        }
    }
    // const changeHandler = (value) => {
    //     onChange(value)
    // }
    console.log("printing dataSRc",dataSrc);
    return (
        <Select
            {...fieldDecoratorProps}
            showSearch = {true}
            showArrow = {true}
            labelInValue={false}
            placeholder = {placeholder || "--select--"}
            mode = {mode || "default"}
            filterOption={false}
            style={{ width: "100%" }}
            notFoundContent = {fetching? <Spin size="small"/>: null}
            onSearch = {debounce(search, 300)}
            // onChange = {changeHandler}
        >
            {
                dataSrc && dataSrc.length && 
                map(dataSrc, (item,index) => (
                <Option
                    key = {item["value"]}
                    title = {item["value"]}
                >
                    {item["label"]}
                </Option>
            ))}
        </Select>
    )
}

export default memo(WorkflowTypeAhead);