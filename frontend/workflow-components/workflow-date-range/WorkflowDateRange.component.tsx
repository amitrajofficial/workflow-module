import React, {FC, memo, useState} from "react";
import { DatePicker } from "antd";
import moment from "moment";
// import * as moment from "moment";
import _ from 'lodash';
const { RangePicker } = DatePicker;
const WorkflowDateRange: FC<any> = ({
    fieldDecoratorProps,
    type,
    form,
    ref
}) => {
    let {options} = type;
    const { displayFormat, disableBetween,enableBetween } = options;
    const {value,onChange, ...rest }  = fieldDecoratorProps;
    const [dateValue,setDateValue] = useState(value);
    const disabledDateCondition = (current) => {
        if(enableBetween) {
            let forward; 
            let backward;
            if(enableBetween.forward)
                forward = current >= (moment as any)().add(_.get(enableBetween,"forward.value",null), _.get(enableBetween,"forward.type","days"))
            if(enableBetween.back)
                backward = current <= (moment as any)().subtract(_.get(enableBetween,"back.value",null), _.get(enableBetween,"back.type","days"))

            return (
                current && (forward || backward)
                )
        }
        else if( disableBetween) {
            let forward = null; 
            let backward = null;
            if(disableBetween.forward)
                forward = current <= (moment as any)().add(_.get(disableBetween,"forward.value",null), _.get(disableBetween,"forward.type","days")).endOf("day")
            if(disableBetween.back)
                backward = current >= (moment as any)().subtract(_.get(disableBetween,"back.value",null), _.get(disableBetween,"back.type","days")).endOf("day")
            
            if(forward!==null && backward!== null)
                return (current && forward && backward)
            else if(forward!==null)
                return (current && forward)
            else if(backward!==null)
                return (current && backward)
        }
        
        return ( null )
    }
    const onChangeHandler = (event) => {
        if(dateValue !== value)
            onChange(dateValue);
    }
    return (
        <RangePicker
            value = {dateValue || null}
            showTime = {false}
            onChange = {setDateValue}
            onBlur = {onChangeHandler}
            format={displayFormat || undefined}
            disabledDate = {disabledDateCondition}
            ref = {ref}
            allowClear = {false}
            {...rest}
        />
    );
};
export default memo(WorkflowDateRange);
