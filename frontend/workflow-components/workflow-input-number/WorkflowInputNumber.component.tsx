import React, { FC, memo, useState } from "react";

import { InputNumber } from "antd";

const WorkflowInputNumber: FC<any> = ({
    fieldDecoratorProps,
    type,
    form,
    ref
}) => {
    let { options } = type;
    let {value, onChange, ...rest} = fieldDecoratorProps;
    let [textValue,setTextValue] = useState(value || "");
    const onChangeHandler = (event) => {
        onChange(event.target.value);
    };
    return (
        <>
            <InputNumber
                {...rest}
                value = {textValue || ""}
                onChange = {(num)=>{
                    setTextValue(num);
                }}
                onBlur = {onChangeHandler}
                hidden={options.hidden || false}
                className="margin-top-5"
                readOnly={options.readOnly || false}
                disabled={options.disabled || false}
                ref = {ref}
                style={{ width: '100%' }}
            />
        </>
    );
};

export default memo(WorkflowInputNumber);
