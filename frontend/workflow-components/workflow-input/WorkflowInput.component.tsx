import React, { FC, memo, useState } from "react";
import { Input } from "antd";

const WorkflowInput: FC<any> = ({
    fieldDecoratorProps,
    type,
    form,
    ref
}) => {
    let { options } = type;
    let {value, onChange, ...rest} = fieldDecoratorProps;
    let [textValue,setTextValue] = useState(value || "");
    const onChangeHandler = (event) => {
        onChange(event.target.value);
    };
    return (
        <>
            <Input
                {...rest}
                value = {textValue || ""}
                onChange = {(event)=>{
                    setTextValue(event.target.value);
                }}
                onBlur = {onChangeHandler}
                type="text"
                hidden={options.hidden || false}
                className="margin-top-5"
                readOnly={options.readOnly || false}
                disabled={options.disabled || false}
                ref = {ref}
            />
        </>
    );
};

export default memo(WorkflowInput);
