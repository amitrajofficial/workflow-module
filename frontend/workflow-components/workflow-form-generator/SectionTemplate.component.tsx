import { Col, Collapse, Row } from 'antd';
import React, { FC, memo } from 'react';

import { ISectionTemplate } from './interfaces';
import { memoize, get } from 'lodash';
import { noop } from "redux-saga/utils";
import { Fields } from './Fields.component';

const { Panel } = Collapse;
export const SectionTemplate: FC<ISectionTemplate> = memoize(
  (props) => {
    const { cssClass, horizontalSpan, collapsable } = props;
    const CollapseField = memo(CollapsableFields);
    const TemplateField = memo(Template);
    return (
      <Col
        className={cssClass || ""}
        span={horizontalSpan || 24}
      >
        {
          collapsable &&
          <CollapseField {...props}/>
        }
        {
          !collapsable &&
          <TemplateField {...props} />
        }
      </Col>
    )
  }
);

const CollapsableFields: FC<ISectionTemplate> = ({ order, title, children, cssClass }) => (
  <Collapse
    bordered={false}
    defaultActiveKey={['1']}
    expandIconPosition="right"
    className={cssClass}
  >
    <Panel
      header={title}
      key={order}
    >
      <Row>
        <Col span={24}>
          {children}
        </Col>
      </Row>
    </Panel>
  </Collapse>
);

const Template:FC<ISectionTemplate> = ({
  cssClass,
  horizontalSpan,
  isRequired,
  title,
  before,
  after,
  children,
  collapsable,
  order,
  titleSpan,
  titleClass,
  form
}) => (
    <Row type="flex" justify="space-around" align="middle" className="margin-bottom-10">

      {before ? (
        <Col className="begin" span={24}>
          {
            typeof(before) === "string" ? before :
              // <SectionTemplate {...before} key={get(before, 'key', '')} form={form}>
                <Fields
                  type={get(before, 'type', null)}
                  mappingKey={get(before, 'key', '') + ""}
                  customValidation={get(before, 'validator', null)}
                  defaultValue={get(before, 'value', '')}
                  form={form}
                />
              // </SectionTemplate>
          }
        </Col>
      ) : (
          noop()
        )}
      {title && title !== "" ? (
        <Col span={titleSpan || 24} style={{ lineHeight: '20px' }} className={titleClass}>
          {title}{isRequired ? "*" : noop()}
        </Col>
      ) : (
          noop()
        )}
      <Col span={horizontalSpan || titleSpan ? 24 - titleSpan : 24}>
        {children}
      </Col>

      {after ? (
        <Col span={24}>
          {
            typeof(after) === "string" ? after :
              // <SectionTemplate {...before} key={get(before, 'key', '')} form={form}>
                <Fields
                  type={get(after, 'type', null)}
                  mappingKey={get(after, 'key', '') + ""}
                  customValidation={get(after, 'validator', null)}
                  defaultValue={get(after, 'value', '')}
                  form={form}
                />
              // </SectionTemplate>
            }
        </Col>
      ) : noop()}
    </Row>
  )

export default memo(SectionTemplate);