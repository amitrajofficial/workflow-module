import { Button, Col, Form, Row } from 'antd';
import React, { FC, memo, useEffect, useState } from 'react';

import { GenericFieldRender } from './FieldRenderer.component';
import { IWorkflowFormGenerator } from './interfaces';
import { handleSecondaryButtonClick } from 'common/workflow-utils/handleSecondaryButtons';
import validationMessages from './validations';

/**
 *  @param {any} hiddenValues is used to pass important values from one step to other
 *  hiddenValues will be in resolver function on workflow-node step as an arguement
 */
const handleSubmit = (
    e,
    validateFieldsAndScroll,
    customData,
    changeIsFormSubmitting,
    onSubmit,
    next,
    submitButtonText,
    hiddenValues
) => {
    e.preventDefault();
    validateFieldsAndScroll((err, values) => {
        if (!err) {
            onSubmit(
                {...values, selectedResponse: submitButtonText },
                next,
                hiddenValues 
            )
        }
    })
}

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

export const WorkflowFormGenerator: FC<IWorkflowFormGenerator> = (props) => {
    const {
        onSubmit,
        form,
        customData,
        submitButtonText,
        secondaryButtons,
        next,
        loading,
        closeModal,
        schema,
        disableAutomaticClose,
        hiddenValues
    } = props;
    const [ isFormSubmitting, changeIsFormSubmitting ] = useState(false);
    const { validateFieldsAndScroll, getFieldsError } = form;
    useEffect(()=> {
        loading? changeIsFormSubmitting(true): changeIsFormSubmitting(false);
        if(!schema && ! next && !disableAutomaticClose){
            setTimeout(() =>{closeModal()}, 3000)
        }
        return () => changeIsFormSubmitting(false); 
    })
    return (
            <Form
                onSubmit = {
                    e => handleSubmit(
                        e,
                        validateFieldsAndScroll,
                        customData,
                        changeIsFormSubmitting,
                        onSubmit,
                        next,
                        submitButtonText,
                        hiddenValues
                    )
                }
            >
                <Row className="workflow-form-padding">
                    <GenericFieldRender {...props} />
                </Row>
            {
                // 
                <Row className="paddedForm">
                    <Col span={24}>
                        <Row gutter={16} type="flex" justify="center" align="middle">
                            {(next !== null) ?
                            <Col>
                                <Button
                                    disabled={isFormSubmitting || hasErrors(getFieldsError())}
                                    htmlType="submit"
                                    type="primary"
                                    loading={loading === submitButtonText}
                                >
                                    { submitButtonText || "Submit" }
                                </Button>
                            </Col>
                            : null
                            }
                            {
                                secondaryButtons && secondaryButtons.length > 0 &&
                                secondaryButtons.map((button, index) => (
                                    <Col key={button.title}>
                                      <Button
                                        htmlType="button"
                                        disabled={isFormSubmitting}
                                        onClick={e => (
                                            button.action &&
                                            handleSecondaryButtonClick(button, props)
                                        )}
                                        style={button.style}
                                        type={button.type || 'default'}
                                        key={button.title + "_" + index}
                                        loading={loading === button.title}
                                      >
                                        {button.title || "..."}
                                      </Button>
                                    </Col>
                                    )
                                )
                            }
                        </Row>
                        
                    </Col>
                </Row>
            }
            </Form>
    )
};

export default memo(
    Form.create<IWorkflowFormGenerator>({
        name: 'workflow',
        validateMessages: validationMessages,
    })(memo(WorkflowFormGenerator))
);