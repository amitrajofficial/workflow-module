import { Col, Row } from 'antd';
import React, { FC, memo } from 'react';
import { get, keys, map, memoize, sortBy } from 'lodash';

import Fields from './Fields.component';
import { IWorkflowFieldRenderer } from './interfaces';
import SectionTemplate from './SectionTemplate.component';

const getSectionsKeys = (schema) => {
    const sectionKeys = keys(schema).join(" ").match(/section_\w+/gmi);
    const orderedSchema = sortBy(sectionKeys, [(key) => schema[key] && schema[key].order]);
    return orderedSchema;
}

export const GenericFieldRender: FC<IWorkflowFieldRenderer> = memoize(
(props) => {
    const { schema, validationSchema, values, form, cssClass } = props;
    if (!schema || Object.keys(schema).length === 0) {
        return <></>;
    }
    const sectionKeys = getSectionsKeys(schema);
    return (
        <SectionTemplate {...schema}>
            <Row className={cssClass}>
                <Col>
                    <Row>
                        {
                            map(schema.fields, function(field, index) {
                                return (
                                    <SectionTemplate {...field} key={ `${get(field, 'key', '')}_${index}` } form={form}>
                                        <Fields
                                            type={ get(field, 'type', null) }
                                            mappingKey={ get(field, 'key', '') }
                                            validationSchema={ validationSchema }
                                            customValidation={ get(field, 'validator', null) }
                                            defaultValue={ get(values, get(field, 'key'), '') }
                                            form={form}
                                        />
                                    </SectionTemplate>
                            )})
                        }
                    </Row>
                </Col>
                <Col>
                    {
                        sectionKeys && sectionKeys.length > 0 && map(sectionKeys, (key, index) => {
                            const newProps = {...props};
                            newProps['schema'] = get(schema, String(key));
                            newProps['values'] = get(schema, `${key}.values`);
                            newProps['validationSchema'] = get(schema, `${key}.validators`);
                            newProps['cssClass'] = get(schema, `${key}.cssClass`, null);
                            newProps['customData'] = get(schema, `${key}.customData`, null);
                            return (
                                <Row key={ String(key) + String(index) } className={newProps['cssClass']}>
                                    <Col className="section">
                                        <GenericFieldRender
                                            {...newProps}
                                        />
                                    </Col>
                                </Row>
                            )}
                        )
                    }
                </Col>
            </Row>
        </SectionTemplate>
    )
    }
);

export default memo(GenericFieldRender);
