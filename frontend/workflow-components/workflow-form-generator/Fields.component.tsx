import React, { FC, forwardRef, memo } from 'react';

import { Form } from 'antd';
import { IField } from './interfaces';
import { WithFallback } from "common/workflow-components"
import getComponent from "common/workflow-components/getComponent";

const { Item } = Form;

export const Fields: FC<IField> = 
({
    type,
    mappingKey,
    validationSchema,
    customValidation,
    defaultValue,
    handleValueChange,
    form
}) => {
    const { getFieldDecorator, getFieldError } = form;
    const { options } = type;
    const { rule } = options;
    const Field = getComponent(type);
    const FieldWithFallBack = (props, ref) => {
        const propsToPass = { fieldDecoratorProps: props, type, form };
        return (
            <WithFallback>
                <div ref={ref}>
                    <Field {...propsToPass} />
                </div>
            </WithFallback>
        )
    };
    const WithForwardRef = forwardRef(FieldWithFallBack);
    const error = getFieldError(mappingKey);
    return (
        <Item
            validateStatus={error ? 'error' : ''}
            help={error || ''}
            style={{ marginBottom: 0 }}
        >
            {
                getFieldDecorator(
                    typeof mappingKey === 'string' ? mappingKey : JSON.stringify(mappingKey),
                    {
                        initialValue: defaultValue || options.initialValue,
                        rules: rule || [],
                        valuePropName: options.valuePropName || "value"
                    }
                )(
                    <WithForwardRef />
                )
            }
        </Item>
    )
};

export default memo(Fields);