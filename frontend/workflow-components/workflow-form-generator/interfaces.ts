export interface IWidgetType {
    widget: string;
    options?: any;
}

export interface IFormFieldSchema {
    title: string | JSX.Element; // control label
    isRequired?: boolean; // indicates whether the given field is mandatory
    horizontalSpan?: number; // horizontal span for the wrapper; antd uses a 24 col grid system
    key: string; // unique key for identifying the control in the form
    type?: IWidgetType; // type of control; eg: inputbox, password box, drop down, etc
    cssClass?: string; // css class to be applied to the wrapper of this form control
    before?: JSX.Element; // component to display before the field
    after?: any; // component to display after the field
    dependents?: Array<string>; // dependent fields for this fieldx
    handleValueChange?: (
      v: any,
      k: string,
      values?: any,
      fieldChange?: (fieldName: string, value: any) => void
    ) => any;
}

export interface IFormSectionSchema {
      title?: string | JSX.Element;
      cssClass?: string;
      horizontalSpan?: number;
      begin?: JSX.Element;
      end?: JSX.Element;
      fields: Array<IFormFieldSchema> | Array<IFormSectionSchema>;
      name?: string;
      values?: any;
      validators?: any;
}

interface IFormSection {
    [section: string]: IFormSectionSchema;
}

export interface IWorkflowFormGenerator {
    schema: IFormSectionSchema | IFormSectionSchema & IFormSection;
    onSubmit: Function;
    validationSchema?: any;
    values?: any;
    form: any;
    customData?: any;
    errors?: any;
    apiErrors?: any;
    cssClass?: string;
    setFieldValue?: any;
    setTouched?: any;
    touched?: any;
    isFormSubmitting?: boolean;
    submitButtonText?: string;
    secondaryButtons?: Array<ISecondaryButton>;
    changeIsFormSubmitting?: Function;
    closeModal: Function;
    next?: string;
    loading?: string;
    status?: "success" | "info" | "warning";
    disableAutomaticClose?: boolean;
    hiddenValues?: any;
}

export interface ISectionTemplate {
    cssClass?: string;
    horizontalSpan?: number;
    title?: string | JSX.Element;
    before?: JSX.Element;
    after?: JSX.Element;
    children: JSX.Element;
    isRequired?: boolean;
    order?: number;
    collapsable?: boolean;
    titleSpan?: number;
    information?: string;
    titleClass?: string;
    form?
}

export interface IField {
    type?: IWidgetType;
    mappingKey: string;
    validationSchema?: any;
    customValidation?: () => void;
    defaultValue?: string;
    form: any;
    handleValueChange?: (currentValue: any, key: string, getFieldsValue: Function, setFields: Function) => void;
    getFieldsValue?: (fieldNames?: Array<string>) => {
        [field: string]: any;
    };
}

export interface IWidgetType {
    widget: string;
    options?: any;
}

export interface ISecondaryButton {
    title?: string;
    style?: object;
    type?: "link" | "default" | "ghost" | "primary" | "dashed" | "danger";
    action?: {
        name: "submit" | "fill" | "hide" | "close";
        values?: object;
        sectionsToHide?: Array<string>;
        next?: string;
    }
}

export interface IWorkflowFieldRenderer {
    schema: IFormSectionSchema | IFormSectionSchema & IFormSection;
    validationSchema?: any;
    values?: any;
    form: any;
    customData?: any;
    cssClass?: string;
}