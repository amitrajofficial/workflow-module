import React, { FC, memo } from "react";

import { Checkbox } from "antd";
// import { CheckboxChangeEvent } from "antd/lib/checkbox";

const WorkflowCheckbox: FC<any> = ({
  fieldDecoratorProps,
  type
}) => {
  const { options } = type;
  const { items } = options;
  
  return <Checkbox.Group
    {...fieldDecoratorProps}
    style = {{width: "100%" }}
    options = {items}
  >
    {options.label}
  </Checkbox.Group>;
};
export default memo(WorkflowCheckbox);
