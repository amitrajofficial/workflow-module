import { Button, Icon } from 'antd';
import React, { FC, memo } from 'react';

export const WorkflowCloseButton: FC<{ closeModal: () => void, loading: string }> = ({ closeModal, loading }) => (
    <Button htmlType="button" onClick={() => closeModal() } loading={loading == 'close'}>
        Close
        <Icon type="close" />
    </Button>
);

export default memo(WorkflowCloseButton)