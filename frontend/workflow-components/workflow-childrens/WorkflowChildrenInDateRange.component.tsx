import React, {FC} from 'react';

import Fields from 'common/workflow-components/workflow-form-generator/Fields.component';
import SectionTemplate from 'common/workflow-components/workflow-form-generator/SectionTemplate.component';
import { get } from 'lodash';
import moment from "moment";

const WorkflowHandleScreeningYears: FC<any> = ({
    options,
    form,
}) => {
    const { children, parent, connection_values, fallbackChildren } = options;
    const { getFieldValue, getFieldError } = form;
    let fieldVal = getFieldValue(parent.key) || get(parent, 'type.options.initialValue', null);
    const current = getFieldValue(parent.key);

    if (
        parent.key &&
        fieldVal && (!getFieldError(parent.key) || getFieldError.length === 0) &&
        moment(current).isBetween(
            (moment as any)().subtract(get(connection_values,"max",null), "years"),
            (moment as any)().subtract(get(connection_values,"min",null), "years")
        ) &&
        children && children.length > 0
    ) {
        return (
            <>
                {
                    children.map((child, i) => (
                        <SectionTemplate {...child} key={ get(child, 'key', '') }>
                            <Fields
                                type={ get(child, 'type', null) }
                                mappingKey={ get(child, 'key', '') }
                                customValidation={ get(child, 'validator', null) }
                                form={form}
                            />
                        </SectionTemplate>
                    ))
                }
            </>
        )
    } else if (
        parent.key &&
        fieldVal && (!getFieldError(parent.key) || getFieldError.length === 0) &&
        fallbackChildren && fallbackChildren.length > 0
    ) {
        return (
            <>
                {
                    fallbackChildren.map((child, i) => (
                        <SectionTemplate {...child} key={ get(child, 'key', '') }>
                            <Fields
                                type={ get(child, 'type', null) }
                                mappingKey={ get(child, 'key', '') }
                                customValidation={ get(child, 'validator', null) }
                                form={form}
                            />
                        </SectionTemplate>
                    ))
                }
            </>
        )
    }
    return null;
};

export default WorkflowHandleScreeningYears;