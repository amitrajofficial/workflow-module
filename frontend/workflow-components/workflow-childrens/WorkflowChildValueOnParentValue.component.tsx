import React, { FC } from 'react';
import { SectionTemplate } from '../workflow-form-generator/SectionTemplate.component';
import { Fields } from '../workflow-form-generator/Fields.component';
import { get } from 'lodash';

// import moment from "moment";

const WorkflowChildValueOnParentValue: FC<any> = ({
    options,
    form,
}) => {
    const { parent, children } = options;
    let { childValueCorrespondToParentValue } = options;
    const { getFieldValue } = form;
    const parentValue = parent && parent.key && getFieldValue(parent.key);

    if (
        parentValue &&
        children && children.length > 0
    ) {
        return (
            <>
                {
                    children.map((child, i) => {
                        child.value = childValueCorrespondToParentValue && childValueCorrespondToParentValue[parentValue];
                        return (<SectionTemplate {...child} key={get(child, 'key', '')} form={form}>
                            <Fields
                                type={get(child, 'type', null)}
                                mappingKey={get(child, 'key', '')}
                                customValidation={get(child, 'validator', null)}
                                form={form}
                                defaultValue = {child.value}
                            />
                        </SectionTemplate>);
                    })
                }
            </>
        )
    }
    else {
        return null;
    }
}


export default WorkflowChildValueOnParentValue;