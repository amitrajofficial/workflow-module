import React, {FC} from 'react';

import Fields from 'common/workflow-components/workflow-form-generator/Fields.component';
import SectionTemplate from 'common/workflow-components/workflow-form-generator/SectionTemplate.component';
import { get } from 'lodash';

const WorkflowHandleScreeningYears: FC<any> = ({
    options,
    form,
}) => {
    const { children, parent, connection_values, fallbackChildren, cssClass } = options;
    const { getFieldValue } = form;
    const years = getFieldValue(`${parent.key}_year`);
    const months = getFieldValue(`${parent.key}_month`);
    let converted_month;
    if(years !== null || months !== null){
        converted_month = ((parseInt( years ) || 0) * 12 + (parseInt( months ) || 0));
    }
    if(connection_values && (connection_values.min || connection_values.max) && !isNaN(converted_month)) {
        const min = connection_values.min || 0;
        const max = connection_values.max || Infinity;

        if(converted_month >= min && converted_month <= max) {
            return (
                <>
                    {
                        children && children.length > 0 && children.map((child, i) => (
                            <SectionTemplate {...child} key={ get(child, 'key', '') } cssClass= {cssClass}>
                                <Fields
                                    type={ get(child, 'type', null) }
                                    mappingKey={ get(child, 'key', '') }
                                    customValidation={ get(child, 'validator', null) }
                                    form={form}
                                />
                            </SectionTemplate>
                        ))
                    }
                </>
            )
        } else {
            return (
                <>
                    {
                        fallbackChildren && fallbackChildren.length > 0 && fallbackChildren.map((child, i) => (
                            <SectionTemplate {...child} key={ get(child, 'key', '') }>
                                <Fields
                                    type={ get(child, 'type', null) }
                                    mappingKey={ get(child, 'key', '') }
                                    customValidation={ get(child, 'validator', null) }
                                    form={form}
                                />
                            </SectionTemplate>
                        ))
                    }
                </>
            )
        }
    }
    return null;
};

export default WorkflowHandleScreeningYears;