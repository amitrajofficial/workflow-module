import React, { FC } from 'react';
import Fields from 'common/workflow-components/workflow-form-generator/Fields.component';
import SectionTemplate from 'common/workflow-components/workflow-form-generator/SectionTemplate.component';
import { get } from 'lodash';
// import moment from "moment";

const WorkflowHandleScreeningYears: FC<any> = ({
    options,
    form,
}) => {
    const { children, parent } = options;
    let { childOptionsCorrespondToParentValue } = options;
    const { getFieldValue, isFieldTouched } = form;
    const parentValue = parent && parent.key && getFieldValue(parent.key);
    if (childOptionsCorrespondToParentValue && childOptionsCorrespondToParentValue[parentValue] && Array.isArray(childOptionsCorrespondToParentValue[parentValue])) {
        childOptionsCorrespondToParentValue[parentValue] = childOptionsCorrespondToParentValue[parentValue].map(option => {
            if (typeof (option) === "string" || typeof (option) === "number") {
                return {
                    label: option,
                    value: option
                }
            } else {
                return option;
            }
        });
    }
    if (parent && parent.key
        && isFieldTouched(parent.key)
        && children && children.length > 0
    ) {
        return (
            <>
                {
                    children.map((child, i) => {
                        if (child && child.type && child.type.options) {
                            child.type.options.items = childOptionsCorrespondToParentValue[parentValue];
                        }
                        return (<SectionTemplate {...child} key={get(child, 'key', '')} form={form}>
                            <Fields
                                type={get(child, 'type', null)}
                                mappingKey={get(child, 'key', '')}
                                customValidation={get(child, 'validator', null)}
                                form={form}
                            />
                        </SectionTemplate>
                        )
                    })
                }
            </>
        )
    }
    else {
        return null;
    }
}

export default WorkflowHandleScreeningYears;