import React, { FC, memo } from 'react';
import { forEach, get, isEqual } from 'lodash';

import Fields from 'common/workflow-components/workflow-form-generator/Fields.component';
import SectionTemplate from 'common/workflow-components/workflow-form-generator/SectionTemplate.component';

export const WorkflowSaveChildrenToParent:FC<any> = ({
    options,
    form,
    fieldDecoratorProps
}) => {
    const { children, parent } = options;
    const { getFieldValue, isFieldsTouched } = form;
    const childrenKeys = children.map(child => child.key);
    const obj = {};
    forEach(children, child => {
        obj[child.key] = getFieldValue(child.key)
    });
    if(isFieldsTouched(childrenKeys) && !isEqual(getFieldValue(parent.key), obj)) {
        var setParent = {};
        setParent[parent.key] = obj;
    }
    
    return(
         <>
            {
                children.map((child, i) => (
                    <SectionTemplate {...child} key={ get(child, 'key', '') }>
                        <Fields
                            type={ get(child, 'type', null) }
                            mappingKey={ get(child, 'key', '') }
                            customValidation={ get(child, 'validator', null) }
                            form={form}
                        />
                    </SectionTemplate>
                ))
            }
        </>
    )
}

export default memo(WorkflowSaveChildrenToParent);
