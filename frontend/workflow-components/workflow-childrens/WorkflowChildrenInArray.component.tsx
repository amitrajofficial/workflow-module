import React, { FC, memo } from 'react';
import { difference, get, isArray } from 'lodash';

import Fields from 'common/workflow-components/workflow-form-generator/Fields.component';
import SectionTemplate from 'common/workflow-components/workflow-form-generator/SectionTemplate.component';

const WorkflowChildrenInArray:FC<any> = ({
    options,
    form
}) => {
    const { parent, children, connection_values, fallbackChildren } = options;
    const { getFieldValue, getFieldError } = form;

    let fieldVal = getFieldValue(parent.key) || get(parent, 'type.options.initialValue', null);
    fieldVal = isArray(connection_values) && !isArray(fieldVal) && fieldVal ? [fieldVal] : fieldVal;
    
    if (
        parent.key &&
        fieldVal && (!getFieldError(parent.key) || getFieldError.length === 0) &&
        (
            difference(fieldVal, connection_values).length === 0 ||
            difference(connection_values, fieldVal).length === 0 ||
            (isArray(connection_values) && connection_values.length === 0 )
        ) &&
        children && children.length > 0
    ) {
        return (
            <>
                {
                    children.map((child, i) => (
                        <SectionTemplate {...child} key={ get(child, 'key', '') }>
                            <Fields
                                type={ get(child, 'type', null) }
                                mappingKey={ get(child, 'key', '') }
                                customValidation={ get(child, 'validator', null) }
                                form={form}
                            />
                        </SectionTemplate>
                    ))
                }
            </>
        )
    } else if (
        parent.key &&
        fieldVal &&
        fallbackChildren && fallbackChildren.length > 0
    ) {
        return (
            <>
                {
                    fallbackChildren.map((child, i) => (
                        <SectionTemplate {...child} key={ get(child, 'key', '') }>
                            <Fields
                                type={ get(child, 'type', null) }
                                mappingKey={ get(child, 'key', '') }
                                customValidation={ get(child, 'validator', null) }
                                form={form}
                            />
                        </SectionTemplate>
                    ))
                }
            </>
        )
    }
    return null;
};

export default memo(WorkflowChildrenInArray);
