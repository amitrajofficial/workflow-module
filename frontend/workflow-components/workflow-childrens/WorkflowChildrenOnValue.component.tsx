import React, { FC, memo } from 'react';
import { isArray, isEqual, sortBy } from 'lodash';

import GenericFieldRender from 'common/workflow-components/workflow-form-generator/FieldRenderer.component';

const WorkflowChildrenOnValue:FC<any> = ({
    options,
    form
}) => {
    const { parent, children, connection_values } = options;
    const { getFieldValue, isFieldTouched } = form;
    let fieldVal = getFieldValue(parent.key);
    fieldVal = isArray(connection_values) && !isArray(getFieldValue(parent.key)) ? [JSON.stringify(getFieldValue(parent.key))] : getFieldValue(parent.key);
    fieldVal = isArray(fieldVal) ? sortBy(fieldVal) : fieldVal;
    fieldVal = typeof fieldVal === 'string' ? fieldVal.toLowerCase() : fieldVal;

    let con_values = isArray(connection_values) ? sortBy(connection_values) : connection_values;
    con_values = typeof con_values === 'string' ? con_values.toLowerCase() : con_values;
    if (
        parent.key &&
        isEqual(con_values, fieldVal) &&
        isFieldTouched(parent.key) &&
        children && children.length > 0
    ) {
        return (
            <>
                {
                    children.map((child, i) => (
                        <GenericFieldRender
                            schema={{ fields: [{...child}] }}
                            form={form}
                            key={i}
                        />
                    ))
                }
            </>
        )
    }
    return null;
};

export default memo(WorkflowChildrenOnValue);
