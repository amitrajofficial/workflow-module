import React, { lazy } from 'react';

import { WithFallback } from 'common/workflow-components/index';

const WorkflowChildrenOnValue = lazy(() => import('./WorkflowChildrenOnValue.component'));
const WorkflowChildrenOnFallback = lazy(() => import('./WorkflowChildrenWithFallback.component'));
const WorkflowSaveChildrenToParent = lazy(() => import('./WorkflowSaveChildrenToParent.component'));
const WorkflowHandleScreeningYears = lazy(() => import('./WorkflowHandleScreeningYears.component'));
const WorkflowChildrenInRange = lazy(() => import('./WorkflowChildrenInRange.component'));
const WorkflowChildrenInArray = lazy(() => import('./WorkflowChildrenInArray.component'));
const WorkflowChildrenInDateRange = lazy(() => import('./WorkflowChildrenInDateRange.component'));
const WorkflowDropOptionsOnParentValue = lazy(() => import('./WorkflowDropOptionsOnParentValue.component'));
const WorkflowChildValueOnParentValue = lazy(() => import('./WorkflowChildValueOnParentValue.component'));

const ShowChildrenOnValue = (props) => (
    <WithFallback>
        <WorkflowChildrenOnValue {...props} />
    </WithFallback>
);

const ShowFallbackChildren = props => (
    <WithFallback>
        <WorkflowChildrenOnFallback {...props} />
    </WithFallback>
);

const SaveChildrenToParent = props => (
    <WithFallback>
        <WorkflowSaveChildrenToParent {...props} />
    </WithFallback>
);

const HandleScreeningYears = props => (
    <WithFallback>
        <WorkflowHandleScreeningYears {...props} />
    </WithFallback>
);

const ShowInRangeWithFallback = props => (
    <WithFallback>
        <WorkflowChildrenInRange {...props} />
    </WithFallback>
);

const ShowInArrayWithFallback = props => (
    <WithFallback>
        <WorkflowChildrenInArray {...props} />
    </WithFallback>
);

const ShowChildrenInDateRange = props => (
    <WithFallback>
        <WorkflowChildrenInDateRange {...props} />
    </WithFallback>
);

const makeOptionsOnParentValue = props => (
    <WithFallback>
        <WorkflowDropOptionsOnParentValue {...props} /> 
    </WithFallback>
);

const childValueOnParentValue = props => (
    <WithFallback>
        <WorkflowChildValueOnParentValue {...props}/>
    </WithFallback>
);

const handleDependency = {
    "SHOW_CHILDREN_ON_SPECIFIC_PARENT_VALUE": ShowChildrenOnValue,
    "SHOW_CHILDREN_WITH_FALLBACK": ShowFallbackChildren,
    "SAVE_CHILDREN_TO_PARENT": SaveChildrenToParent,
    "HANDLE_SCREENING_YEARS": HandleScreeningYears,
    "SHOW_CHILDREN_IN_RANGE_WITH_FALLBACK": ShowInRangeWithFallback,
    "SHOW_CHILDREN_IF_IN_ARRAY": ShowInArrayWithFallback,
    "SHOW_CHILDREN_IN_DATE_RANGE": ShowChildrenInDateRange,
    "MAKE_DROP_OPTIONS_ON_PARENT_VALUE": makeOptionsOnParentValue,
    "CHILD_VALUE_DEPENDENT_ON_PARENT": childValueOnParentValue
};

export default handleDependency;