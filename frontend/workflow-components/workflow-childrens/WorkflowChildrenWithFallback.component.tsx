import React, { FC, memo } from 'react';
import { get, isArray, isEqual, sortBy } from 'lodash';

import Fields from 'common/workflow-components/workflow-form-generator/Fields.component';
import SectionTemplate from 'common/workflow-components/workflow-form-generator/SectionTemplate.component';

// import isEqual from 'deep-equal';

const WorkflowChildrenOnValue:FC<any> = ({
    options,
    form
}) => {
    const { parent, children, connection_values, fallbackChildren } = options;
    const { getFieldValue, getFieldError } = form;

    let fieldVal = getFieldValue(parent.key) || get(parent, 'type.options.initialValue', null);
    fieldVal = isArray(connection_values) && !isArray(fieldVal) && fieldVal ? [fieldVal] : fieldVal;
    fieldVal = isArray(fieldVal) ? sortBy(fieldVal) : fieldVal;
    fieldVal = typeof fieldVal === 'string' ? fieldVal.toLowerCase() : fieldVal;

    let con_values = isArray(connection_values) ? sortBy(connection_values) : connection_values;
    con_values = typeof con_values === 'string' ? con_values.toLowerCase() : con_values;
    if (
        parent.key &&
        fieldVal && (!getFieldError(parent.key) || getFieldError.length === 0) &&
        (isEqual(con_values, fieldVal) || (isArray(connection_values) && connection_values.length === 0 ) )&&
        children && children.length > 0
    ) {
        return (
            <>
                {
                    children.map((child, i) => (
                        <SectionTemplate {...child} key={ get(child, 'key', '') }>
                            <Fields
                                type={ get(child, 'type', null) }
                                mappingKey={ get(child, 'key', '') }
                                customValidation={ get(child, 'validator', null) }
                                form={form}
                            />
                        </SectionTemplate>
                    ))
                }
            </>
        )
    } else if (
        parent.key &&
        fieldVal && (!getFieldError(parent.key) || getFieldError.length === 0) &&
        fallbackChildren && fallbackChildren.length > 0
    ) {
        return (
            <>
                {
                    fallbackChildren.map((child, i) => (
                        <SectionTemplate {...child} key={ get(child, 'key', '') }>
                            <Fields
                                type={ get(child, 'type', null) }
                                mappingKey={ get(child, 'key', '') }
                                customValidation={ get(child, 'validator', null) }
                                form={form}
                            />
                        </SectionTemplate>
                    ))
                }
            </>
        )
    }
    return null;
};

export default memo(WorkflowChildrenOnValue);
