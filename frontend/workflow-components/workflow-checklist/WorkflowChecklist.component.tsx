import * as React from "react";

import { Checkbox } from "antd";
import { CheckboxValueType } from "antd/lib/checkbox/Group";
const CheckboxGroup = Checkbox.Group;
const WorkflowChecklist = props => {
  const { value } = props;
  const { items } = props.options;
  function onChange(checkedValues: Array<CheckboxValueType>) {
    props.form.setFieldValue(props.field.name, checkedValues);
  }
  return items instanceof Array ? (
    <CheckboxGroup
      onChange={onChange}
      options={items}
      defaultValue={value ? (value instanceof Array ? value : [value]) : []}
    >
      {props.label}
    </CheckboxGroup>
  ) : null;
};
export default WorkflowChecklist;
