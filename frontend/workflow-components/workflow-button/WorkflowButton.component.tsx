import React, {FC, memo} from 'react';

import { Button } from 'antd';

const WorkflowButton:FC<any> = ({ type }) => {
    const { options } = type;
    return(
        <Button {...options}>
            {options.text}
        </Button>
    )
};

export default memo(WorkflowButton);