import React, { FC, memo, useState } from 'react';

import { Input } from 'antd';
import { get } from 'lodash';

const { TextArea } = Input;

const WorkflowTextArea: FC<any> = ({
    fieldDecoratorProps,
    type,
    form,
    ref
}) => {
    let { options } = type;
    let { characterLimit, hidden, readOnly, disabled} = options;
    let {value, onChange, ...rest} = fieldDecoratorProps;
    let [textValue,setTextValue] = useState(value || "");
    const onChangeHandler = (event) => {
        onChange(event.target.value);
    };
    return (
        <>
            <TextArea
                {...rest}
                value = {textValue}
                hidden={hidden || false}
                readOnly={readOnly || false}
                disabled={disabled || false}
                onChange = {(event) => setTextValue(event.target.value)}
                onBlur = {onChangeHandler}
                rows={options.rows || 3}
                maxLength={characterLimit}
                ref = {ref}
            />
            {
                characterLimit ?
                    <p style={{ float: "right", color: "#696969"}}>
                        Character Count {get(textValue, "length", 0)}/{characterLimit}
                    </p> : null
            }
        </>
    )
}

export default memo(WorkflowTextArea);