export interface IComponentProps {
    fieldDecoratorProps: any;
    form: any;
    type: {
        widget: string;
        rule?: Array<object>;
        options?: any;
    };
    ref: any;
}