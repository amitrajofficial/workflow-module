import { Col, Row, Typography } from 'antd';
import React, { FC, memo } from 'react';

import { IWorkflowTitle } from './interfaces';
import { WorkflowGoBackButton } from 'common/workflow-components';

const { Title } = Typography;

const WorkflowTitle: FC<IWorkflowTitle> = ({ prev, title, goBack, loading }) => (
    <Row type="flex" justify="start" align="middle">
      <Col span={5}>
        {
          prev &&
          <WorkflowGoBackButton
            goBack={goBack}
            loading={loading}
          />
        }
      </Col>
      <Col span={19}>
        <Title level={4} ellipsis className="text-center text-capitalize" style={{ margin: "0.5em 0px" }}>
          { title }
        </Title>
      </Col>
    </Row>
);

export default memo(WorkflowTitle);