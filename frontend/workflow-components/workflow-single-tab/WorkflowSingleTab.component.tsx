import React, { FC, memo } from 'react';
import { WorkflowCloseButton, WorkflowIndividualTab } from 'common/workflow-components';

import  { Card } from 'antd';
import { IWorkflowSingleTab } from './interfaces';
import WorkflowTitle from './WorkflowTitle.component';

const WorkflowSingleTabCard: FC<IWorkflowSingleTab> = ({ goBack, workflowTab, onSubmit, closeModal, loading }) => (
    <Card
      title={
        <WorkflowTitle goBack={goBack} title={workflowTab.title} prev={workflowTab.prev} loading={loading} />
      }
      bordered={false}
      extra={
        <WorkflowCloseButton
          closeModal={closeModal}
          loading={loading}
        />
      }
      bodyStyle={{ padding: 0 }}
    >
      <WorkflowIndividualTab
        slice={workflowTab}
        onSubmit={onSubmit}
        closeModal={closeModal}
        loading = {loading}
      />
    </Card>
)


export default memo(WorkflowSingleTabCard);