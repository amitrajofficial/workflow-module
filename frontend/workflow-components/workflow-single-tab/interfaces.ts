export interface IWorkflowResponseData {
    formSchema: any;
    validationSchema: any;
    values: any;
    information?: string | null;
    next: string | null;
    title?: string | number | null;
    prev?: any;
    error?: {
      type: string;
      message: string;
    }
}

export interface IWorkflowSingleTab {
    goBack: (event) => void;
    workflowTab: IWorkflowResponseData;
    onSubmit: Function;
    closeModal: () => void;
    loading?: string;
}

export interface IWorkflowTitle {
  prev: string;
  title: string | number | null;
  goBack: (event) => void;
  loading: string
}