import React, { FC, memo } from "react";

import { Radio } from "antd";
// import { CheckboxChangeEvent } from "antd/lib/checkbox";

const WorkflowRadioButton: FC<any> = ({
  fieldDecoratorProps,
  type
}) => {
  const { options } = type;
  const { items } = options;
  
  return <Radio.Group
    {...fieldDecoratorProps}
    style = {{width: "100%" }}
    options = {items}
  >
    {options.label}
  </Radio.Group>;
};
export default memo(WorkflowRadioButton);
