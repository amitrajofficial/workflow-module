import React, { FC, memo } from 'react';
import { isEqual, uniqWith } from 'lodash';

import { Select } from 'antd';

const { Option } = Select;

const WorkflowDropDown: FC<any> = ({
    fieldDecoratorProps,
    type,
    form,
    ref
}) => {
    const { options } = type;
    const { items, mode, labelKey, valueKey } = options;
    const uniqueItems = uniqWith(items, isEqual);
    return (
        <Select
            {...fieldDecoratorProps}
            showSearch={true}
            style={{ width: "100%" }}
            tokenSeparators={[',']}
            mode={mode || "default"}
            ref={ref}
            filterOption
        >
            {
                uniqueItems && uniqueItems.map(item => (
                    <Option
                        key={item[valueKey || "value"]}
                        title={item[labelKey || "label"]}
                        value={item[valueKey || "value"]}
                    >
                        {item[labelKey || "label"]}
                    </Option>
                ))
            }
        </Select>
    )
};

export default memo(WorkflowDropDown);