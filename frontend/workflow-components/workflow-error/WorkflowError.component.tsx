import { Button, Card, Col, Row } from 'antd';
import React, { FC, memo } from 'react';

import { WorkflowCloseButton } from 'common/workflow-components';
import { WorkflowErrorTypes } from './interfaces';
import WorkflowTitle from 'common/workflow-components/workflow-single-tab/WorkflowTitle.component';
import { get as _get } from 'lodash';

export const WorkflowError: FC<WorkflowErrorTypes> = ({ goBack, errorTemplate, loading, closeModal }) => {
    const err = _get(errorTemplate, 'error', null);
    const prev = _get(errorTemplate, 'prev', null);
    return (
      <Card
        title={
          <WorkflowTitle goBack={goBack} title= { err.title || "Something went wrong"} prev={prev} loading={loading} />
        }
        bordered={false}
        extra={
          <WorkflowCloseButton
            closeModal={closeModal}
            loading={loading}
          />
        }
        bodyStyle={{ padding: 25 }}
      >
        <Row type="flex" justify="center">
           <Col span={24}>
             <p className="text-danger">
               Message: { `${JSON.stringify(err.type)} : ${JSON.stringify(err.message)}` }
             </p>
           </Col>
           {loading? 
           <Button loading>Loading...</Button>
           : null}
         </Row>
      </Card>
    )
};

export default memo(WorkflowError);