export interface WorkflowErrorTypes {
    goBack: (event: any ) => void;
    errorTemplate: {
        error: {
          type: string | number;
          message?: string
        };
        information?: string;
        prev: {
          name: string;
          values: { [propName: string]: any };
        } | null;
    };
    loading?: string;
    closeModal: () => void;
}