import React, {FC, memo, useState} from "react";
import { TimePicker, Button } from "antd";
// import moment from "moment";
// import * as moment from "moment";

const WorkflowTimePicker: FC<any> = ({
    fieldDecoratorProps,
    type,
    form,
    ref
}) => {
    let {options} = type;
    const { displayFormat } = options;
    const {value,onChange, ...rest }  = fieldDecoratorProps;
    const [timeValue, setTimeValue] = useState(value);
    const onChangeHandler = () => {
        onChange(timeValue);
    }
    return (
        <TimePicker
            value = {timeValue || null}
            onChange = {setTimeValue}
            addon={() => (
                <Button size="small" type="primary" onClick={onChangeHandler}>
                  Ok
                </Button>
              )}
              format={displayFormat || undefined}
              ref = {ref}
              allowClear = {false}
              {...rest}
        />
    );
};
export default memo(WorkflowTimePicker);
