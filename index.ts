import IO from "./backend/server/common/socket";
import eventListeners from "./backend/server/common/event-listeners/eventListeners";
import WorkFlowForm from "./frontend/workflow-forms/WorkflowForm.component";


export function StartSocket(server, config){
    const ioConnection = new IO().createConnection(server);
    ioConnection.onConnection(function(socket) {
        eventListeners.socketConnection(socket, eventListeners.workflowSocket);
    });
    
}

export default WorkFlowForm;